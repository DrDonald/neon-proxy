package io.neonproxy.common.config.impl;

import io.neonproxy.common.config.GsonHelper;
import io.neonproxy.common.config.IConfiguration;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.function.Supplier;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
public class JsonConfiguration<T> implements IConfiguration<T> {

    private final File file;
    private final Class<T> tClass;
    private T document;

    public JsonConfiguration(final File file, final Class<T> tClass) {
        this.tClass = tClass;
        this.file = file;
    }

    @Override
    public IConfiguration<T> createIfNotExists(Supplier<T> supplier) {
        if (this.file == null || this.file.isDirectory())
            throw new IllegalArgumentException("File is null or directory");

        if (!this.file.exists()) {
            if (this.file.getParentFile() != null) {
                this.file.getParentFile().mkdirs();
            }
            try {
                this.file.createNewFile();
                this.write(supplier.get());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return this;
    }

    @Override
    public IConfiguration<T> load() {
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(this.file));
            this.document = GsonHelper.PRETTY_GSON.fromJson(reader, this.tClass);
            reader.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return this;
    }

    @Override
    public IConfiguration<T> write(T document) {
        this.document = document;
        return this.save();
    }

    @Override
    public IConfiguration<T> save() {
        try {
            final Writer writer = new OutputStreamWriter(new FileOutputStream(this.file), StandardCharsets.UTF_8);

            GsonHelper.PRETTY_GSON.toJson(this.document, writer);

            writer.flush();
            writer.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return this;
    }

    @Override
    public boolean exists() {
        return this.file.exists();
    }

    @Override
    public IConfiguration<T> continueIfExists() {
        if(this.exists()) {
            return this;
        }

        return new EmptyConfiguration<>();
    }

    @Override
    public IConfiguration<T> continueIfDoesntExists() {
        if(!this.exists()) {
            return this;
        }

        return new EmptyConfiguration<>();
    }

    @Override
    public T get() {
        return this.document;
    }
}
