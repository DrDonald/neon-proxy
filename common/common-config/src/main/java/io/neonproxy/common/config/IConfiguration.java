package io.neonproxy.common.config;

import io.neonproxy.common.config.impl.EmptyConfiguration;

import java.util.function.Supplier;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public interface IConfiguration<T> {

   IConfiguration<T> createIfNotExists(final Supplier<T> supplier);

   IConfiguration<T> load();

   IConfiguration<T> write(T type);

   IConfiguration<T> save();

   boolean exists();

   /**
    * @return this instance if config exists. Otherwise it'll return {@link EmptyConfiguration} for no further affects.
    */
   IConfiguration<T> continueIfExists();

   /**
    * @return this instance if config does not exist. Otherwise it'll return {@link EmptyConfiguration} for no further affects.
    */
   IConfiguration<T> continueIfDoesntExists();

   T get();
}
