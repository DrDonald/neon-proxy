package io.neonproxy.common.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public class GsonHelper {

    public static final File PLUGIN_ROOT;

    static {
        PLUGIN_ROOT = new File("plugins//NeonProxy//");
        PLUGIN_ROOT.mkdirs();
    }

    public static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();
    public static final Gson PRETTY_GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

    public static JsonElement fromPath(JsonObject object, String path) {
        if(object == null) {
            return null;
        }

        String[] seg = path.split("\\.");
        for(String element : seg) {
            JsonElement ele = object.get(element);
            if(!ele.isJsonObject()) {
                return ele;
            }
            object = ele.getAsJsonObject();
        }

        return object;
    }

    public static String fromPathAsString(JsonObject object, String path) {
        if(object == null) {
            return "null";
        }

        String[] seg = path.split("\\.");
        for(String element : seg) {
            JsonElement ele = object.get(element);
            if(!ele.isJsonObject()) {
                return ele.getAsString();
            }
            object = ele.getAsJsonObject();
        }

        return object.getAsString();
    }
}
