package io.neonproxy.common.module;

import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.LifecycleHook;
import io.neonproxy.common.module.api.ModuleDescription;
import io.neonproxy.common.module.api.ModuleHost;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
@AllArgsConstructor
public class ModuleHostImpl implements ModuleHost {

    @Getter
    private final AbstractModule moduleInstance;

    @Override
    public ModuleDescription getDescription() {
        return this.moduleInstance.getModuleDescription();
    }

    @Override
    public void callLifecycle(ModuleLifecycle moduleLifecycle) {
        this.getLifecycleMethods(moduleLifecycle)
                .forEach(method -> {
                    try {
                        method.setAccessible(true);
                        method.invoke(this.moduleInstance);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
    }

    private List<Method> getLifecycleMethods(ModuleLifecycle moduleLifecycle) {
        List<Method> lifecycleMethods = new LinkedList<>();

        Method[] methods = this.moduleInstance
                .getClass()
                .getDeclaredMethods();

        for (Method method : methods) {
            LifecycleHook hook;
            if ((hook = method.getAnnotation(LifecycleHook.class)) != null && hook.state() == moduleLifecycle) {
                lifecycleMethods.add(method);
            }
        }

        lifecycleMethods.sort(
                Comparator.comparingInt(method ->
                        method.getAnnotation(LifecycleHook.class).potency()
                )
        );

        return lifecycleMethods;
    }
}
