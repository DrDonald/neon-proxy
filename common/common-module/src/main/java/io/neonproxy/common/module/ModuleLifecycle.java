package io.neonproxy.common.module;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
public enum ModuleLifecycle {

    LOADED,
    ENABLED,
    DISABLED,
    UNLOAD,
}
