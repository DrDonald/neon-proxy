package io.neonproxy.common.module;

import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.ModuleDescription;
import io.neonproxy.common.module.api.ModuleHost;
import io.neonproxy.common.module.api.ModuleManager;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
@Getter
public class ModuleClassLoader extends URLClassLoader {

    private final File file;

    private final ModuleDescription moduleDescription;
    private final JarFile jarFile;
    private ModuleHost moduleHost;

    public ModuleClassLoader(File file) throws IOException {
        super(new URL[]{file.toURI().toURL()});

        this.file = file;

        this.jarFile = new JarFile(this.file);
        this.moduleDescription = this.findModuleDescription(jarFile);
    }

    public void loadModule(ModuleManager moduleManager) throws Exception {
        Class<?> main = this.loadClass(this.moduleDescription.getMain());

        AbstractModule module = (AbstractModule) main.getDeclaredConstructor()
                .newInstance();

        module.setModuleDescription(this.moduleDescription);
        module.setClassLoader(this);
        module.setModuleManager(moduleManager);

        ModuleHost host = new ModuleHostImpl(module);
        host.callLifecycle(ModuleLifecycle.LOADED);
        this.moduleHost = host;
    }

    public void unloadModule() throws IOException {
        this.moduleHost.callLifecycle(ModuleLifecycle.UNLOAD);

        moduleHost.getModuleInstance().getClassLoader()
                .getJarFile().close();

        this.close();
    }

    private ModuleDescription findModuleDescription(JarFile jarFile) throws IOException {
        JarEntry description = jarFile.getJarEntry("module.properties");

        if (description == null) {
            throw new IOException("Module " + jarFile.getName() + " wasn't loaded. Every neon-proxy module should contain a module.properties file.");
        }

        try(InputStreamReader reader = new InputStreamReader(jarFile.getInputStream(description), StandardCharsets.UTF_8)) {
            Properties properties = new Properties();
            properties.load(reader);

            return new ModuleDescription(properties);
        }
    }
}
