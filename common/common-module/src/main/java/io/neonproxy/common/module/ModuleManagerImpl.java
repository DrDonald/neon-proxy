package io.neonproxy.common.module;

import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.ModuleManager;
import io.neonproxy.common.module.api.ModuleHost;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
public class ModuleManagerImpl implements ModuleManager {

    private final static transient File MODULES_FOLDER;

    static {
        MODULES_FOLDER = new File("./modules");
        MODULES_FOLDER.mkdirs();
    }

    //TODO Remove this shit
    public static void main(String[] args) {
        new ModuleManagerImpl();
    }

    private final Map<String, ModuleHost> modules = new LinkedHashMap<>();

    public ModuleManagerImpl() {
        this.loadModules();

        this.unloadAllModules();
    }

    private void loadModules() {
        Map<ModuleHost, Boolean> toEnable = new LinkedHashMap<>();

        for (File file : Objects.requireNonNull(MODULES_FOLDER.listFiles(f -> f.isFile() && f.getName().endsWith(".jar")))) {
            try {
                ModuleClassLoader loader = new ModuleClassLoader(file);
                loader.loadModule(this);

                this.modules.put(loader.getModuleDescription().getName(), loader.getModuleHost());
                toEnable.put(loader.getModuleHost(), false);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Error loading module " + file.getName());
                e.printStackTrace();
            }
        }

        toEnable.forEach((host, b) -> this.enableModule(toEnable, host, new Stack<>()));
    }

    public void unloadAllModules() {
        this.modules.forEach((n, moduleHost) -> {
            moduleHost.callLifecycle(ModuleLifecycle.DISABLED);
            moduleHost.getModuleInstance().setEnabled(false);
            try {
                System.out.println("Disabling module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
                moduleHost.getModuleInstance().getClassLoader().unloadModule();
                System.out.println("Successfully disabled module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error while disabling module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
            }
        });

        modules.clear();
    }

    private void enableModule(Map<ModuleHost, Boolean> toEnable, ModuleHost moduleHost, Stack<ModuleHost> dependantStack) {
        if (toEnable.get(moduleHost)) {
            return;
        }

        moduleHost.getDescription()
                .getDependencies()
                .remove(moduleHost.getDescription().getName());

        if (dependantStack.contains(moduleHost)) {
            StringBuilder dependencyBuilder = new StringBuilder();

            dependantStack.forEach(host -> dependencyBuilder.append(host.getDescription().getName()).append(" -> "));

            System.err.println("Error while enabling module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
            System.err.println("Circular dependency detected: " + dependencyBuilder);

            try {
                moduleHost.getModuleInstance().getClassLoader().unloadModule();
            } catch (IOException e) {
                e.printStackTrace();
            }

            this.modules.remove(moduleHost.getDescription().getName());
            return;
        }

        for (String dependencyName : moduleHost.getDescription().getDependencies()) {
            this.getModule(dependencyName)
                    .ifPresentOrElse(module -> {
                        dependantStack.push(moduleHost);
                        this.enableModule(toEnable, moduleHost, dependantStack);
                        dependantStack.pop();
                    }, () -> System.err.println("Error while loading module " + moduleHost.getDescription().getName() + ": Dependency" + dependencyName + " not found."));
        }

        try {
            System.out.println("Enabling module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor() + "...");

            moduleHost.callLifecycle(ModuleLifecycle.ENABLED);
            moduleHost.getModuleInstance().setEnabled(true);

            toEnable.put(moduleHost, true);

            System.out.println("Successfully enabled module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error while enabling module " + moduleHost.getDescription().getName() + " v" + moduleHost.getDescription().getVersion() + " by " + moduleHost.getDescription().getAuthor());
        }
    }

    @Override
    public Set<AbstractModule> getModules() {
        return this.modules
                .values()
                .stream()
                .map(ModuleHost::getModuleInstance)
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<AbstractModule> getModule(String name) {
        ModuleHost moduleHost = this.modules.get(name);

        if (moduleHost == null) {
            return Optional.empty();
        }

        return Optional.of(moduleHost.getModuleInstance());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AbstractModule> Optional<T> getModuleByType(Class<T> type) {
        for (ModuleHost host : this.modules.values()) {
            if (host.getModuleInstance().getClass() == type) {
                return (Optional<T>) Optional.ofNullable(host.getModuleInstance());
            }
        }

        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Optional<T> getModuleByInterface(Class<T> type) {
        for (ModuleHost host : this.modules.values()) {
            Class<?>[] interfaces = host.getModuleInstance().getClass().getInterfaces();

            if(interfaces.length > 0 && interfaces[0] == type) {
                return (Optional<T>) Optional.ofNullable(host.getModuleInstance());
            }
        }

        return Optional.empty();
    }

    @Override
    public void reload() {
        throw new UnsupportedOperationException("Reloading modules currently isn't supported.");
    }
}
