package io.neonproxy.common.module.api;

import io.neonproxy.common.module.ModuleClassLoader;
import io.neonproxy.common.config.GsonHelper;
import io.neonproxy.common.config.IConfiguration;
import io.neonproxy.common.config.impl.JsonConfiguration;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
@Getter @Setter
public abstract class AbstractModule {

    private ModuleManager moduleManager;

    private ModuleDescription moduleDescription;

    private ModuleClassLoader classLoader;

    private boolean enabled;

    public <T> IConfiguration<T> prepareConfig(String name, Class<T> type) {
        return new JsonConfiguration<>(new File(GsonHelper.PLUGIN_ROOT, "//configs//" + name + ".json"), type);
    }

    public InputStream getResource(String filename) throws IOException {
        if (filename == null) {
            throw new IllegalArgumentException("Filename cannot be null");
        }

        URL url = getClass().getClassLoader().getResource(filename);
        if (url == null) {
            return null;
        }

        URLConnection connection = url.openConnection();
        connection.setUseCaches(false);
        return connection.getInputStream();
    }
}
