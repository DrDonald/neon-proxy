package io.neonproxy.common.module.api;

import java.util.Optional;
import java.util.Set;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
public interface ModuleManager {

    Set<AbstractModule> getModules();

    Optional<AbstractModule> getModule(String name);
    <T extends AbstractModule> Optional<T> getModuleByType(Class<T> type);
    <T> Optional<T> getModuleByInterface(Class<T> type);

    void reload();
}
