package io.neonproxy.common.module.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
public class ModuleDescription {

    private final String name;
    private final String author;
    private final String version;
    private final String main;
    private final List<String> dependencies;
    private String description = "A blank neon proxy module.";

    public ModuleDescription(Properties properties) {
        this.name = properties.getProperty("name");
        this.author = properties.getProperty("author");
        this.version = properties.getProperty("version");
        this.main = properties.getProperty("main");

        String rawDependencies = properties.getProperty("dependencies");

        if(rawDependencies.isBlank()) {
            this.dependencies = Collections.emptyList();
        } else {
            String[] split = rawDependencies.split(";");
            this.dependencies = new ArrayList<>();
            for(String dependency : split) {
                if(!dependency.isBlank()) {
                    this.dependencies.add(dependency);
                }
            }
        }

        this.description = properties.getProperty("description");
    }
}
