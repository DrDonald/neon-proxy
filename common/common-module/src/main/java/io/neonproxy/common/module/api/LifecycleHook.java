package io.neonproxy.common.module.api;

import io.neonproxy.common.module.ModuleLifecycle;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LifecycleHook {

    ModuleLifecycle state();

    short potency() default 16;
}
