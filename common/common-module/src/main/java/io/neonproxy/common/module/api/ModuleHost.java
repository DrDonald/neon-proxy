package io.neonproxy.common.module.api;

import io.neonproxy.common.module.ModuleLifecycle;

/**
 * @author Jasper Stritzke
 * @since 29.07.21
 */
public interface ModuleHost {

    ModuleDescription getDescription();

    AbstractModule getModuleInstance();

    void callLifecycle(ModuleLifecycle moduleLifecycle);
}
