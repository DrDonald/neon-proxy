package io.neonproxy.common.module.event.api;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 21.01.21
 * Copyright (c) 2021
 */
public class EventPriority {

    public static final int FIRST = 0;
    public static final int SECOND = 25;
    public static final int DEFAULT = 50;
    public static final int SECOND_LAST = 75;
    public static final int LAST = 100;
}
