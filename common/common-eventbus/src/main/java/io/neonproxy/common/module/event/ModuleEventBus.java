package io.neonproxy.common.module.event;

import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.event.api.EventBus;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
@Getter
public class ModuleEventBus extends AbstractModule {

    private EventBus eventBus;
}
