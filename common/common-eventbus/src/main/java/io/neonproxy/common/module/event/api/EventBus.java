package io.neonproxy.common.module.event.api;

import io.neonproxy.common.module.api.AbstractModule;

import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 17.05.21
 */
public interface EventBus {

    void registerListener(Object o, AbstractModule moduleKey);

    void unregisterListener(Object o);

    void unregisterListener(AbstractModule moduleKey);

    void callEvent(final Event event);

    void callEventAndExecuteIfNotCanceled(CancelableEvent event, Consumer<CancelableEvent> eventConsumer);
}
