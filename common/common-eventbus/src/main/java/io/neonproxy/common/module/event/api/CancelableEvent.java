package io.neonproxy.common.module.event.api;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Jasper Stritzke
 * @since 17.05.21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public abstract class CancelableEvent extends Event {

    private boolean canceled;
}
