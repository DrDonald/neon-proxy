package io.neonproxy.common.module.event.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 21.01.21
 * Copyright (c) 2021
 */
@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.METHOD )
public @interface Handler {

    /**
     * A list of event classes, the handler is supposed to listen to
     */
    Class<? extends Event>[] handles() default {};

    /**
     * If true, handler will be executed async
     */
    boolean async() default false;

    int priority() default 50;
}
