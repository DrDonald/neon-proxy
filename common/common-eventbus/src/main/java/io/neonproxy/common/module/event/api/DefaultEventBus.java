package io.neonproxy.common.module.event.api;

import io.neonproxy.common.module.api.AbstractModule;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 17.05.21
 */
public class DefaultEventBus implements EventBus {

    private final Map<Class<? extends Event>, List<RegisteredHandler>> listeners;
    private final ExecutorService service = Executors.newCachedThreadPool();

    public DefaultEventBus() {
        this.listeners = new HashMap<>();
    }

    @Override
    public void registerListener(Object listener, AbstractModule moduleKey) {
        for (Method method : listener.getClass().getDeclaredMethods()) {
            final Handler handler = method.getAnnotation(Handler.class);

            if (handler == null) {
                continue;
            }

            if (method.getParameterCount() != 1) {
                continue;
            }

            this.registerHandler(moduleKey, listener, method, handler);
        }
    }

    @Override
    public void unregisterListener(AbstractModule moduleKey) {
        for(Class<? extends Event> eventType : listeners.keySet()) {
            listeners
                    .get(eventType)
                    .removeIf(handler -> handler.getModuleKey() == moduleKey);
        }
    }

    @Override
    public void unregisterListener(Object listener) {
        this.listeners.values()
                .forEach(listeners -> listeners.removeIf(l -> l.listener.equals(listener)));
    }

    @SuppressWarnings("unchecked")
    private void registerHandler(AbstractModule moduleKey, Object listener, Method method, Handler handler) {
        if (handler.handles().length == 0) {
            if (method.getParameterCount() == 1) {
                this.register(moduleKey, (Class<? extends Event>) method.getParameterTypes()[0], listener, method, handler);
            }
        }

        for (Class<? extends Event> clazz : handler.handles()) {
            this.register(moduleKey, clazz, listener, method, handler);
        }

        this.sortListeners();
    }

    private void register(AbstractModule moduleKey, Class<? extends Event> clazz, Object listener, Method method, Handler handler) {
        if (!this.listeners.containsKey(clazz)) {
            this.listeners.put(clazz, new LinkedList<>());
        }

        this.listeners
                .get(clazz)
                .add(
                        new RegisteredHandler(
                                moduleKey,
                                method,
                                listener,
                                handler.async(),
                                handler.priority()
                        )
                );
    }

    private void sortListeners() {
        for (List<RegisteredHandler> handlers : this.listeners.values()) {
            handlers.sort(
                    Comparator.comparingInt(handler -> handler.priority)
            );
        }
    }

    @Override
    public void callEvent(Event event) {
        final List<RegisteredHandler> list = this.listeners.get(event.getClass());

        if (list == null || list.isEmpty()) {
            return;
        }

        list.forEach(handler -> {
            if (handler.isAsync()) {
                this.service.submit(() -> handler.invoke(event));
                return;
            }

            handler.invoke(event);
        });
    }

    @Override
    public void callEventAndExecuteIfNotCanceled(CancelableEvent event, Consumer<CancelableEvent> eventConsumer) {
        this.callEvent(event);

        if(!event.isCanceled()) {
            eventConsumer.accept(event);
        }
    }

    @AllArgsConstructor
    private static class RegisteredHandler {

        @Getter
        private final AbstractModule moduleKey;

        private final Method method;
        private final Object listener;

        @Getter
        private final boolean async;
        private final int priority;

        public void invoke(Event event) {
            try {
                this.method.invoke(listener, event);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                System.err.println("Could not fire event: " + e.getMessage());
            }
        }
    }
}