# NeonProxy

---
**A scalable, modular and fully standalone proxy-system to moderate players and prevent bots**
*Compatible with BungeeCord (and most forks) & Velocity Proxy*

## Basic Idea

A proxy plugin working on BungeeCord & velocity to manage bots, bans (including chat-bans), chat-logs, support,
auto-broadcast and all easily scalable and controllable using a neat web interface made in Vue3 + Vuetify BETA.

**Everything is modular. Every single module can be activated or deactivated.**

*For the start of the project, neon-proxy will only support mysql or similar databases. On demand, it's possible to
integrate support for other databases like mongodb*

**Discord:** Jasper#3240

---

### Modules:

* [Punishment](#punishment) (Including chat-bans) - not implemented
    * [Chat-Log](#chat-logs) (Only available if neon-proxy web-server is up and running) - not implemented
* [Anti-Bot](#anti-bot) - not implemented
* [Control-Panel](#control-panel) (Only available if ) - not implemented
* *Feel free to ask for new features :)*

### Module detail:

---

### Punishment

* Temporarily or permanent
* can contain extra text attached (the punished player can see this message if the placeholder [{extra}](#placeholders)
  is used in the kick screen or mute-message)
* can have a link attached

### Chat-Logs

Chat-Logs can be created ingame by every player. They can be reviewed ingame or in the browser using the
self-hosted [control panel](#control-panel)

### Anti-Bot

On its release, neon-proxy should be able to identify most bots and handle bot attacks without lag.

# Control Panel

----

*Text about control-panel*

## Configuration

*You'll find a .PROXY_ID file in every proxy folder, running with neonproxy. It's used to identify and verify a proxy in multi-proxy mode.*

```json
{
  "tls": {
    "enabled": false,
    "file": "./tls.jks"
  },
  "controlPanel": {
    "enabled": false,
    "bridgePort": 4060
  },
  "multiProxy": {
    "enabled": false,
    "bridgePort": 4060,
    "syncBotInfo": true
  },
  "antiBot": {
    "blockSuspects": true
  },
  "database": {
    "hostname": "localhost",
    "database": "database",
    "port": 3306,
    "username": "username",
    "password": "password",
    "tablePrefix": "neonproxy_"
  },
  "language": {
    "tryAdaptLanguage": true,
    "default": "en_EN"
  }
}
```

*Files with **!** will be ignored*

The port of the control-panel-bridge and the multi-proxy-port can be the same. If both is activated, they're sharing
the same endpoint.

---

### Language

Messages and every kind of text a player gets to see is saved in a configuration. On login neon proxy will figure out
the Locale of a player and will use that for messages. The Locale can be changed using the ProxyPlayer#setLocale
method. (If you already have an own language system, you can set the locale on login)

### Placeholders
