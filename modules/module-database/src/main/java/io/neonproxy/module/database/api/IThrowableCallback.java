package io.neonproxy.module.database.api;

import java.sql.ResultSet;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 31.01.21
 * Copyright (c) 2021
 */
public interface IThrowableCallback<T> {

    T call(ResultSet resultSet) throws Throwable;
}
