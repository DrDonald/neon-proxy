package io.neonproxy.module.database.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.CompletableFuture;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
public interface IMySQLManager {

    /**
     * Link to {@link IMySQLConnector#getConnection()}
     * @return a connection from the sql connection pool
     */
    Connection getConnection();

    /**
     * Executes an update statement with the provided query.
     */
    void executeUpdate(String query);

    /**
     * Executes a query statement with the provided query.
     */
    <T> T executeQuery(String query, IThrowableCallback<T> callback);

    /**
     * Executes a query statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    <T> T executeQuery(String query, IThrowableCallback<T> callback, Object... parameters);

    /**
     * Prepares a query statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    <T> T executeQuery(String query, IThrowableCallback<T> callback, String... parameters);

    /**
     * Executes a query statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    <T> CompletableFuture<T> executeQueryAsync(String query, IThrowableCallback<T> callback, Object... parameters);

    /**
     * Prepares a query statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    <T> CompletableFuture<T> executeQueryAsync(String query, IThrowableCallback<T> callback, String... parameters);

    /**
     * Executes an update statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    void executeUpdate(String query, Object... parameters);

    /**
     * Prepares an update statement with the provided query, and directly inserts all parameters
     *
     * @param query the query
     * @param parameters all parameters in correct order.
     */
    void executeUpdate(String query, String... parameters);

    /**
     * Executes a query asynchronously
     * @return a completable future, containing the ResultSet.
     */
    CompletableFuture<ResultSet> executeAsyncQuery(PreparedStatement preparedStatement);

    /**
     * Prepares a statement using query and executes it immediately
     */
    void update(String query);
}
