package io.neonproxy.module.database.sql;

import io.neonproxy.module.database.api.IMySQLConnector;
import io.neonproxy.module.database.api.IMySQLManager;
import io.neonproxy.module.database.api.IThrowableCallback;
import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
@AllArgsConstructor
public class MySQLManager implements IMySQLManager {

    private final IMySQLConnector connector;

    @Override
    public Connection getConnection() {
        return this.connector.getConnection();
    }

    @Override
    public void executeUpdate(String query) {
        try (Connection connection = this.connector.getConnection()) {
            connection.prepareStatement(query).executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public <T> T executeQuery(String query, IThrowableCallback<T> callback) {
        try (Connection connection = this.connector.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return callback.call(resultSet);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }

    @Override
    public <T> T executeQuery(String query, IThrowableCallback<T> callback, Object... parameters) {
        try (Connection connection = this.connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            int counter = 1;
            for (Object parameter : parameters) {
                statement.setObject(counter, parameter);
                counter++;
            }

            try (ResultSet resultSet = statement.executeQuery()) {
                return callback.call(resultSet);
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }

    @Override
    public <T> T executeQuery(String query, IThrowableCallback<T> callback, String... parameters) {
        try (Connection connection = this.connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            int counter = 1;
            for (String parameter : parameters) {
                statement.setString(counter, parameter);
                counter++;
            }

            try (ResultSet resultSet = statement.executeQuery()) {
                return callback.call(resultSet);
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }

    @Override
    public <T> CompletableFuture<T> executeQueryAsync(String query, IThrowableCallback<T> callback, Object... parameters) {
        return CompletableFuture.supplyAsync(() -> this.executeQuery(query, callback, parameters));
    }

    @Override
    public <T> CompletableFuture<T> executeQueryAsync(String query, IThrowableCallback<T> callback, String... parameters) {
        return CompletableFuture.supplyAsync(() -> this.executeQuery(query, callback, parameters));
    }

    @Override
    public void executeUpdate(String query, Object... parameters) {
        try (Connection connection = this.connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            int counter = 1;
            for (Object parameter : parameters) {
                statement.setObject(counter, parameter);
                counter++;
            }

            statement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void executeUpdate(String query, String... parameters) {
        try (Connection connection = this.connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);

            int counter = 1;
            for (String parameter : parameters) {
                statement.setString(counter, parameter);
                counter++;
            }

            statement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * @deprecated
     */
    @Override
    public CompletableFuture<ResultSet> executeAsyncQuery(PreparedStatement preparedStatement) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return preparedStatement.executeQuery();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }

            return null;
        });
    }

    @Override
    public void update(String query) {
        this.executeUpdate(query);
    }
}
