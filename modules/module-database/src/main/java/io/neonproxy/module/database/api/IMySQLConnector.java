package io.neonproxy.module.database.api;

import java.sql.Connection;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
public interface IMySQLConnector {

    Connection getConnection();

    void shutdown();
}
