package io.neonproxy.module.database;

import io.neonproxy.common.module.ModuleLifecycle;
import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.LifecycleHook;
import io.neonproxy.module.database.api.IMySQLManager;
import io.neonproxy.module.database.config.MySQLConfig;
import io.neonproxy.module.database.sql.MySQLConnector;
import io.neonproxy.module.database.sql.MySQLManager;
import io.neonproxy.module.database.api.IMySQLConnector;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
@Getter
public class DatabaseModule extends AbstractModule {

    private IMySQLConnector mySQLConnector;
    private IMySQLManager mySQLManager;

    @LifecycleHook(state = ModuleLifecycle.ENABLED)
    private void handleEnable() {
        MySQLConfig mySQLConfig = this.prepareConfig(
                "database",
                MySQLConfig.class
        ).createIfNotExists(MySQLConfig::new).load().get();

        this.mySQLConnector = new MySQLConnector(mySQLConfig);
        this.mySQLManager = new MySQLManager(mySQLConnector);
    }

    @LifecycleHook(state = ModuleLifecycle.DISABLED)
    private void handleDisable() {
        this.mySQLConnector.shutdown();
    }
}
