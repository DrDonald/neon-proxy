package io.neonproxy.module.database.config;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
@Getter
public class MySQLConfig {

    private String host = "localhost";
    private int port = 3306;
    private String database = "Stats";
    private String username = "username";
    private String password = "password";

    private List<String> optinalRequestParameters = Arrays.asList("useUnicode=true", "characterEncoding=utf8");

    private int poolSize = 15, connectionTimeout = 3000, validationTimeout = 3000, idleTimeout = 30000;
}

