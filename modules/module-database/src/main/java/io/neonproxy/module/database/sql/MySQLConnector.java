package io.neonproxy.module.database.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.neonproxy.module.database.config.MySQLConfig;
import io.neonproxy.module.database.api.IMySQLConnector;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
public class MySQLConnector implements IMySQLConnector {

    private final HikariDataSource dataSource;

    public MySQLConnector(final MySQLConfig config) {
        final StringBuilder optionalParameters = new StringBuilder("?");

        if (config.getOptinalRequestParameters() != null && !config.getOptinalRequestParameters().isEmpty()) {
            for (int index = 0; index < config.getOptinalRequestParameters().size(); index++) {
                final String parameter = config.getOptinalRequestParameters().get(index);

                optionalParameters.append(parameter);
                if (index < config.getOptinalRequestParameters().size() - 1) {
                    optionalParameters.append("&");
                }
            }
        }

        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:mysql://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase() + optionalParameters.toString());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        hikariConfig.setDriverClassName("com.mysql.jdbc.Driver");

        hikariConfig.setConnectionTimeout(config.getConnectionTimeout());
        hikariConfig.setValidationTimeout(config.getValidationTimeout());
        hikariConfig.addDataSourceProperty("useSSL", false); //Change to true and provide truststore if you want to use SSL

        this.dataSource = new HikariDataSource(hikariConfig);
        this.dataSource.validate();
    }

    @Override
    public Connection getConnection() {
        try {
            return this.dataSource.getConnection();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        return null;
    }

    @Override
    public void shutdown() {
        this.dataSource.close();
    }
}
