package io.neonproxy.module.network.channel;

import io.neonproxy.common.config.GsonHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
@Getter
@AllArgsConstructor
public class PubSubPacketContainer {

    private final String name;
    private final String content;

    public PubSubPacket parse(Class<? extends PubSubPacket> type) {
        return GsonHelper.GSON.fromJson(this.content, type);
    }

    public String toJSONString() {
        return GsonHelper.GSON.toJson(this);
    }

    public static PubSubPacketContainer wrap(PubSubPacket pubSubPacket) {
        return new PubSubPacketContainer(pubSubPacket.getClass().getName(), GsonHelper.GSON.toJson(pubSubPacket));
    }


    public static PubSubPacketContainer fromString(String json) {
        return GsonHelper.GSON.fromJson(json, PubSubPacketContainer.class);
    }
}
