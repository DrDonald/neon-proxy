package io.neonproxy.module.network.local;

import io.neonproxy.module.network.channel.IPubSubChannel;
import io.neonproxy.module.network.channel.IPubSubManager;
import io.neonproxy.module.network.channel.PubSubPacket;
import lombok.AllArgsConstructor;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public class SimulatedPubSubManager implements IPubSubManager {

    //Channel - set of listeners
    private final Map<String, Set<LocalListener>> channels = new HashMap<>();

    @Override
    public void registerPacket(Class<? extends PubSubPacket> pubSubPacket) {
    }

    @Override
    public CompletableFuture<IPubSubChannel> registerChannel(Consumer<PubSubPacket> packetConsumer, String channel) {
        this.channels.putIfAbsent(channel.toUpperCase(), new HashSet<>());

        LocalPubSubChannel localPubSubChannel = new LocalPubSubChannel(channel, this);
        this.channels.get(channel.toUpperCase()).add(
                new LocalListener(this, null, packetConsumer)
        );

        return CompletableFuture.completedFuture(localPubSubChannel);
    }

    @Override
    public CompletableFuture<IPubSubChannel> registerChannel(Object listener, String channel) {
        this.channels.putIfAbsent(channel.toUpperCase(), new HashSet<>());

        LocalPubSubChannel localPubSubChannel = new LocalPubSubChannel(channel, this);
        this.channels.get(channel.toUpperCase()).add(
                new LocalListener(this, listener, null)
        );

        return CompletableFuture.completedFuture(localPubSubChannel);
    }

    @Override
    public boolean existsType(String typeName) {
        return true;
    }

    @Override
    public Class<? extends PubSubPacket> getTypeByName(String name) {
        return PubSubPacket.class;
    }

    private final CompletableFuture<Void> completedFuture = CompletableFuture.runAsync(() -> {
    });

    @Override
    public CompletableFuture<Void> dispatchPacket(String channelName, PubSubPacket pubSubPacket) {
        if (this.channels.containsKey(channelName.toUpperCase())) {
            Set<LocalListener> channelListeners = this.channels.get(channelName.toUpperCase());

            for (LocalListener listener : channelListeners) {
                listener.invoke(pubSubPacket);
            }
        }

        return this.completedFuture;
    }

    @AllArgsConstructor
    protected static class LocalListener {

        private final IPubSubManager pubSubManager;

        private final Object listener;
        private final Consumer<PubSubPacket> packetConsumer;

        public void invoke(PubSubPacket pubSubPacket) {
            if (this.listener != null) {
                this.pubSubManager.invokeListener(this.listener, pubSubPacket);
                return;
            }

            this.packetConsumer.accept(pubSubPacket);
        }
    }
}
