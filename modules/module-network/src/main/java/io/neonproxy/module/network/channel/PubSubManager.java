package io.neonproxy.module.network.channel;

import io.neonproxy.module.network.api.IRedisManager;
import io.neonproxy.module.network.config.RedisConfig;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
public class PubSubManager implements IPubSubManager, AutoCloseable {

    private final Map<String, Class<? extends PubSubPacket>> packets = new ConcurrentHashMap<>();
    private final RedisConfig redisConfig;
    private final Set<IPubSubChannel> channels = new HashSet<>();

    private final IRedisManager redisManager;

    public PubSubManager(RedisConfig config, IRedisManager redisManager) {
        this.redisConfig = config;
        this.redisManager = redisManager;
    }

    @Override
    public Class<? extends PubSubPacket> getTypeByName(String name) {
        return this.packets.getOrDefault(name, PubSubPacket.class);
    }

    @Override
    public CompletableFuture<Void> dispatchPacket(String channelName, PubSubPacket pubSubPacket) {
        return CompletableFuture.runAsync(() -> {
            if (!this.existsType(pubSubPacket.getClass().getName())) {
                System.err.println("Packet " + pubSubPacket.getClass().getName() + " isn't registered and therefore can't be sent.");
                return;
            }

            PubSubPacketContainer wrappedPacket = PubSubPacketContainer.wrap(pubSubPacket);
            String rawJson = wrappedPacket.toJSONString();

            this.redisManager.publish(channelName, rawJson);
        });
    }

    @Override
    public void registerPacket(Class<? extends PubSubPacket> pubSubPacket) {
        this.packets.put(pubSubPacket.getName(), pubSubPacket);
    }

    @Override
    public CompletableFuture<IPubSubChannel> registerChannel(Consumer<PubSubPacket> packetConsumer, String channel) {
        return CompletableFuture.supplyAsync(() -> {
            IPubSubChannel pubSubChannel = new PubSubChannelImpl(channel, this.redisManager, this, this.redisConfig, packetConsumer);
            this.channels.add(pubSubChannel);

            return pubSubChannel;
        });
    }

    @Override
    public CompletableFuture<IPubSubChannel> registerChannel(Object listener, String channel) {
        return CompletableFuture.supplyAsync(() -> {
            IPubSubChannel pubSubChannel = new PubSubChannelImpl(channel, this.redisManager, this, this.redisConfig, listener);
            this.channels.add(pubSubChannel);

            return pubSubChannel;
        });
    }

    @Override
    public boolean existsType(String typeName) {
        return this.packets.containsKey(typeName);
    }

    @Override
    public void close() {
        for (IPubSubChannel channel : this.channels) {
            try {
                channel.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
