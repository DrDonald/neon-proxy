package io.neonproxy.module.network.channel;

import io.neonproxy.module.network.api.PacketHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
public interface IPubSubManager {

    void registerPacket(Class<? extends PubSubPacket> pubSubPacket);

    CompletableFuture<IPubSubChannel> registerChannel(Consumer<PubSubPacket> packetConsumer, String channel);

    CompletableFuture<IPubSubChannel> registerChannel(Object listener, String channel);

    boolean existsType(String typeName);

    Class<? extends PubSubPacket> getTypeByName(String name);

    CompletableFuture<Void> dispatchPacket(String channelName, PubSubPacket pubSubPacket);

    default void invokeListener(Object listener, PubSubPacket pubSubPacket) {
        for(Method method : listener.getClass().getDeclaredMethods()) {
            if(method.isAnnotationPresent(PacketHandler.class) && method.getParameterCount() == 1 && method.getParameterTypes()[0] == pubSubPacket.getClass()) {
                try {
                    method.setAccessible(true);
                    method.invoke(listener, pubSubPacket);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
