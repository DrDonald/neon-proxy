package io.neonproxy.module.network.api;

import java.util.Map;
import java.util.Set;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
public interface IRedisManager {

    //Basic
    void set(String key, String value);

    void set(int db, String key, String value);

    String get(String key);

    String get(int db, String key);

    void hset(String key, String hashKey, String hashValue);

    void hset(int db, String key, String hashKey, String hashValue);

    void hset(String key, Map<String, String> map);

    void hset(int db, String key, Map<String, String> map);

    Map<String, String> hgetAll(String key);

    Map<String, String> hgetAll(int db, String key);

    String hget(String key, String hashKey);

    String hget(int db, String key, String hashKey);

    /**
     * Sets an object, serialized via gson to network
     *
     * @param key   key
     * @param value value
     */
    void setObject(String key, Object value);

    /**
     * Sets an object, serialized via gson, to the selected network
     *
     * @param db    database to be selected
     * @param key   key
     * @param value value
     */
    void setObject(int db, String key, Object value);

    /**
     * Sets an object, serialized via gson to network (with expire)
     *
     * @param key   key
     * @param value value
     * @param ttl   time to live
     */
    void setObject(String key, Object value, long ttl);

    /**
     * Sets an object, serialized via gson, to the selected network (with expire)
     *
     * @param key   key
     * @param value value
     * @param ttl   time to live
     */
    void setObject(int db, String key, Object value, long ttl);

    /**
     * Gets a string from network and converts it to an object using {@param clazz}
     *
     * @param key key
     * @return the object
     */
    <E> E getObject(String key, Class<E> clazz);

    /**
     * Gets a string from network {@param db} and converts it to an object using {@param clazz}
     *
     * @return the object
     */
    <E> E getObject(int db, String key, Class<E> clazz);

    /**
     * Adds a TTL to a key
     *
     * @param key key
     * @param ttl time to live
     */
    void expire(String key, long ttl);

    /**
     * Adds a TTL to a key in the {@param db} database
     *
     * @param key key
     * @param ttl time to live
     */
    void expire(int db, String key, long ttl);

    Set<String> keys(String pattern);

    Set<String> keys(int db, String pattern);

    void publish(String channel, String message);

    void stop();
}
