package io.neonproxy.module.network;

import com.google.gson.Gson;
import io.neonproxy.common.config.GsonHelper;
import io.neonproxy.module.network.api.IRedisManager;
import io.neonproxy.module.network.config.RedisConfig;
import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
public class RedisManager implements IRedisManager {

    private final RedisConfig config;
    private final Gson GSON = GsonHelper.GSON;

    @Getter
    private JedisPool jedisPool;

    public RedisManager(RedisConfig config) {
        this.config = config;

        this.start();
    }

    private JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(this.config.getMaxTotal());
        poolConfig.setMaxIdle(this.config.getMaxIdle());
        poolConfig.setMinIdle(this.config.getMinIdle());

        return poolConfig;
    }

    private void start() {
        this.jedisPool = new JedisPool(this.buildPoolConfig(), this.config.getHost(), this.config.getPort());
    }

    public <T> T callWithRedis(int db, Function<Jedis, T> callback) {
        try (final Jedis jedis = jedisPool.getResource()) {
            jedis.auth(config.getAuthKey());
            jedis.select(db);

            return callback.apply(jedis);
        }
    }

    public <T> T callWithRedis(Function<Jedis, T> callback) {
        try (final Jedis jedis = jedisPool.getResource()) {
            jedis.auth(config.getAuthKey());
            jedis.select(0);

            return callback.apply(jedis);
        }
    }


    public void withRedis(int db, Consumer<Jedis> consumer) {
        try (final Jedis jedis = jedisPool.getResource()) {
            jedis.auth(config.getAuthKey());
            jedis.select(db);

            consumer.accept(jedis);
        }
    }

    public void withRedis(Consumer<Jedis> consumer) {
        try (final Jedis jedis = jedisPool.getResource()) {
            jedis.auth(config.getAuthKey());
            jedis.select(0);

            consumer.accept(jedis);
        }
    }

    @Override
    public void set(String key, String value) {
        this.withRedis(jedis -> jedis.set(key, value));
    }

    @Override
    public void set(int db, String key, String value) {
        this.withRedis(db, jedis -> jedis.set(key, value));
    }

    @Override
    public String get(String key) {
        return this.callWithRedis(jedis -> jedis.get(key));
    }

    @Override
    public String get(int db, String key) {
        return this.callWithRedis(db, jedis -> jedis.get(key));
    }

    @Override
    public void hset(String key, String hashKey, String hashValue) {
        this.withRedis(jedis -> jedis.hset(key, hashKey, hashValue));
    }

    @Override
    public void hset(int db, String key, String hashKey, String hashValue) {
        this.withRedis(db, jedis -> jedis.hset(key, hashKey, hashValue));
    }

    @Override
    public void hset(String key, Map<String, String> map) {
        this.withRedis(jedis -> jedis.hset(key, map));
    }

    @Override
    public void hset(int db, String key, Map<String, String> map) {
        this.withRedis(db, jedis -> jedis.hset(key, map));
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        return this.callWithRedis(jedis -> jedis.hgetAll(key));
    }

    @Override
    public Map<String, String> hgetAll(int db, String key) {
        return this.callWithRedis(db, jedis -> jedis.hgetAll(key));
    }

    @Override
    public String hget(String key, String hashKey) {
        return this.callWithRedis(jedis -> jedis.hget(key, hashKey));
    }

    @Override
    public String hget(int db, String key, String hashKey) {
        return this.callWithRedis(db, jedis -> jedis.hget(key, hashKey));
    }

    @Override
    public void setObject(String key, Object o) {
        this.withRedis(jedis -> jedis.set(key, this.GSON.toJson(o)));
    }

    @Override
    public void setObject(int db, String key, Object o) {
        this.withRedis(db, jedis -> jedis.set(key, this.GSON.toJson(o)));
    }

    @Override
    public void setObject(String key, Object o, long ttl) {
        this.withRedis(jedis -> {
            Pipeline pipeline = jedis.pipelined();

            pipeline.set(key, this.GSON.toJson(o));
            pipeline.expire(key, ttl);

            pipeline.sync();
        });
    }

    @Override
    public void setObject(int db, String key, Object o, long ttl) {
        this.withRedis(jedis -> {
            Pipeline pipeline = jedis.pipelined();
            pipeline.select(db);

            pipeline.set(key, this.GSON.toJson(o));
            pipeline.expire(key, ttl);

            pipeline.sync();
        });
    }

    @Override
    public <E> E getObject(String key, Class<E> clazz) {
        return this.callWithRedis(jedis -> this.GSON.fromJson(jedis.get(key), clazz));
    }

    @Override
    public <E> E getObject(int db, String key, Class<E> clazz) {
        return this.callWithRedis(db, jedis -> this.GSON.fromJson(jedis.get(key), clazz));
    }

    @Override
    public void expire(String key, long ttl) {
        this.withRedis(jedis -> jedis.expire(key, ttl));
    }

    @Override
    public void expire(int db, String key, long ttl) {
        this.withRedis(db, jedis -> jedis.expire(key, ttl));
    }

    @Override
    public Set<String> keys(String pattern) {
        return this.callWithRedis(jedis -> jedis.keys(pattern));
    }

    @Override
    public Set<String> keys(int db, String pattern) {
        return this.callWithRedis(db, jedis -> jedis.keys(pattern));
    }

    @Override
    public void publish(String channel, String message) {
        this.withRedis(jedis -> jedis.publish(channel, message));
    }

    @Override
    public void stop() {
        this.jedisPool.close();
    }
}
