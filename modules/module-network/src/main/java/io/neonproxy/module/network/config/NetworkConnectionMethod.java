package io.neonproxy.module.network.config;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public enum NetworkConnectionMethod {

    SINGLE_HOST,
    REDIS_PUBSUB
}
