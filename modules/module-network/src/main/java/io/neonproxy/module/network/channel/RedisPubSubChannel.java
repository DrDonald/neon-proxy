package io.neonproxy.module.network.channel;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
public class RedisPubSubChannel {

    public static final String GENERAL = "GENERAL";
    public static final String NOTIFY = "NOTIFY";
    public static final String INTERNAL = "_INTERNAL";
}
