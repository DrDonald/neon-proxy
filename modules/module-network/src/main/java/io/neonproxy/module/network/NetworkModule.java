package io.neonproxy.module.network;

import io.neonproxy.common.module.ModuleLifecycle;
import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.LifecycleHook;
import io.neonproxy.module.network.api.IRedisManager;
import io.neonproxy.module.network.api.INetworkModule;
import io.neonproxy.module.network.channel.IPubSubManager;
import io.neonproxy.module.network.channel.PubSubManager;
import io.neonproxy.module.network.config.NetworkConnectionMethod;
import io.neonproxy.module.network.config.NetworkModuleConfig;
import io.neonproxy.module.network.local.SimulatedPubSubManager;
import io.neonproxy.module.network.local.SimulatedRedisManager;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 06.08.21
 */
@Getter
public class NetworkModule extends AbstractModule implements INetworkModule {

    private IRedisManager redisManager;
    private IPubSubManager pubSubManager;

    @LifecycleHook(state = ModuleLifecycle.ENABLED)
    public void handleEnable() {
        NetworkModuleConfig networkModuleConfig = this.prepareConfig(
                "redis",
                NetworkModuleConfig.class
        ).createIfNotExists(NetworkModuleConfig::new).load().get();

        if (networkModuleConfig.getConnectionMethod() == NetworkConnectionMethod.SINGLE_HOST) {
            System.out.println("Initializing networking using local components");

            this.redisManager = new SimulatedRedisManager();
            this.pubSubManager = new SimulatedPubSubManager();
        } else {
            System.out.println("Initializing networking using your redis configuration.");

            this.redisManager = new RedisManager(networkModuleConfig.getRedisConfig());
            this.pubSubManager = new PubSubManager(networkModuleConfig.getRedisConfig(), this.redisManager);
        }

        System.out.println("Initialized networking.");
    }

    @LifecycleHook(state = ModuleLifecycle.DISABLED)
    private void handleDisable() {
        this.redisManager.stop();
    }
}
