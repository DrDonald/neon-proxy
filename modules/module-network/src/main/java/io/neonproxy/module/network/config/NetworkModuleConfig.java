package io.neonproxy.module.network.config;

import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
@Getter
public class NetworkModuleConfig {

    private NetworkConnectionMethod connectionMethod = NetworkConnectionMethod.SINGLE_HOST;
    private RedisConfig redisConfig = new RedisConfig();
}
