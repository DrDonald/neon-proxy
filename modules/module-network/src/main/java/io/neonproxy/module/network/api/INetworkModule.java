package io.neonproxy.module.network.api;

import io.neonproxy.module.network.channel.IPubSubManager;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public interface INetworkModule {

    IRedisManager getRedisManager();
    IPubSubManager getPubSubManager();
}
