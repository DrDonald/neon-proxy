package io.neonproxy.module.network.channel;

import io.neonproxy.module.network.api.IRedisManager;
import io.neonproxy.module.network.config.RedisConfig;
import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public class PubSubChannelImpl implements IPubSubChannel {

    public static final ExecutorService SERVICE = Executors.newCachedThreadPool();

    @Getter
    private final String channelName;
    private final IRedisManager redisManager;
    private final IPubSubManager pubSubManager;

    private final Jedis jedis;

    public PubSubChannelImpl(String channelName, IRedisManager redisManager, IPubSubManager pubSubManager, RedisConfig redisConfig, Object listener) {
        this(channelName, redisManager, pubSubManager, redisConfig);

        this.start(packet -> this.pubSubManager.invokeListener(listener, packet));
    }

    public PubSubChannelImpl(String channelName, IRedisManager redisManager, IPubSubManager pubSubManager, RedisConfig redisConfig, Consumer<PubSubPacket> packetConsumer) {
        this(channelName, redisManager, pubSubManager, redisConfig);

        this.start(packetConsumer);
    }

    public PubSubChannelImpl(String channelName, IRedisManager redisManager, IPubSubManager pubSubManager, RedisConfig redisConfig) {
        this.channelName = channelName;

        this.redisManager = redisManager;
        this.pubSubManager = pubSubManager;

        this.jedis = new Jedis(redisConfig.getHost(), redisConfig.getPort());
        this.jedis.auth(redisConfig.getAuthKey());
    }

    private void start(Consumer<PubSubPacket> packetConsumer) {
        SERVICE.submit(() -> this.jedis.subscribe(new JedisPubSub() {

            @Override
            public void onMessage(String channel, String message) {
                assert !channel.equals(channelName) : "Received message from a channel that wasn't subscribed";

                PubSubPacketContainer pubSubPacketContainer = PubSubPacketContainer.fromString(message);
                PubSubPacket pubSubPacket = pubSubPacketContainer.parse(
                        PubSubChannelImpl.this.pubSubManager.getTypeByName(
                                pubSubPacketContainer.getName()
                        )
                );

                packetConsumer.accept(pubSubPacket);
            }
        }, channelName));
    }

    @Override
    public CompletableFuture<Void> dispatchPacket(PubSubPacket pubSubPacket) {
        return CompletableFuture.runAsync(() -> {

            if (!this.pubSubManager.existsType(pubSubPacket.getClass().getName())) {
                System.err.println("Packet " + pubSubPacket.getClass().getName() + " isn't registered and therefore can't be sent.");
                return;
            }

            PubSubPacketContainer wrappedPacket = PubSubPacketContainer.wrap(pubSubPacket);
            String rawJson = wrappedPacket.toJSONString();

            this.redisManager.publish(this.channelName, rawJson);
        });
    }

    @Override
    public void close() {
        this.jedis.close();
    }
}
