package io.neonproxy.module.network.channel;

import java.util.concurrent.CompletableFuture;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public interface IPubSubChannel extends AutoCloseable {

    String getChannelName();

    CompletableFuture<Void> dispatchPacket(PubSubPacket pubSubPacket);
}
