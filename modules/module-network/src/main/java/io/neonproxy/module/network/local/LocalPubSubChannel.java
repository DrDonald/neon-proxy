package io.neonproxy.module.network.local;

import io.neonproxy.module.network.channel.IPubSubChannel;
import io.neonproxy.module.network.channel.PubSubPacket;
import lombok.AllArgsConstructor;

import java.util.concurrent.CompletableFuture;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
@AllArgsConstructor
public class LocalPubSubChannel implements IPubSubChannel {

    private final String channel;
    private final SimulatedPubSubManager pubSubManager;

    @Override
    public String getChannelName() {
        return this.channel;
    }

    @Override
    public CompletableFuture<Void> dispatchPacket(PubSubPacket pubSubPacket) {
        return this.pubSubManager.dispatchPacket(this.channel, pubSubPacket);
    }

    @Override
    public void close() throws Exception {

    }
}
