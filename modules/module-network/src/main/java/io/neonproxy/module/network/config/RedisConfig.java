package io.neonproxy.module.network.config;

import lombok.Getter;

/**
 * A software created by
 *
 * @author Jasper Stritzke
 * @since 18.01.21
 * Copyright (c) 2021
 */
@Getter
public class RedisConfig {

    private String host = "localhost";
    private int port = 6379;
    private String authKey = "auth-key";

    private int minIdle = 4, maxIdle = 32, maxTotal = 32;
}

