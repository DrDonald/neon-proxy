package io.neonproxy.module.network.local;

import io.neonproxy.module.network.api.IRedisManager;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public class SimulatedRedisManager implements IRedisManager {

    private final static transient SimulatedRedisEntry EMPTY_ENTRY = new SimulatedRedisEntry();

    private final Timer evictionTimer;

    private final Map<Integer, Map<String, SimulatedRedisEntry>> redisEntryMap;

    public SimulatedRedisManager() {
        this.redisEntryMap = new ConcurrentHashMap<>();

        this.evictionTimer = new Timer();
        new Thread(() -> this.evictionTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                SimulatedRedisManager.this.checkEviction();
            }
        }, 1000, 1000)).start();
    }

    private final List<String> evictableKeys = new LinkedList<>();

    private void checkEviction() {
        this.redisEntryMap.forEach((id, entries) -> {
            entries.forEach((key, entry) -> {
                if (entry.isExpired()) {
                    this.evictableKeys.add(key);
                }
            });

            this.evictableKeys.forEach(entries::remove);
            this.evictableKeys.clear();
        });
    }

    //==================================================================================================================

    @Getter
    @Setter
    protected static class SimulatedRedisEntry {

        protected Object objectValue;
        protected Map<String, String> mapValue;

        protected long expiry = -1L;

        public SimulatedRedisEntry() {
        }

        public SimulatedRedisEntry(Object value) {
            this.objectValue = value;
            this.mapValue = null;
        }

        public SimulatedRedisEntry(Map<String, String> value) {
            this.objectValue = null;
            this.mapValue = value;
        }

        public String getAsString() {
            if (this.objectValue instanceof String) {
                return (String) this.objectValue;
            }

            return null;
        }

        public boolean isExpired() {
            return this.expiry != -1L && this.expiry < System.currentTimeMillis();
        }

        public void cancelExpiry() {
            this.expiry = -1L;
        }

        public void expireIn(Duration duration) {
            this.expiry = System.currentTimeMillis() + duration.toMillis();
        }

        public Map<String, String> getMapAndCreateIfNotExists() {
            if (this.mapValue == null) {
                this.mapValue = new HashMap<>();
            }

            return this.mapValue;
        }

        public String getMapKey(String hashKey) {
            return this.mapValue == null ? null : this.mapValue.get(hashKey);
        }

        public <E> E getObjectValue(Class<E> type) {
            if (!type.isInstance(this.objectValue)) {
                return null;
            }

            return type.cast(this.objectValue);
        }
    }

    private SimulatedRedisEntry preSet(String key) {
        return this.preSet(0, key);
    }

    private SimulatedRedisEntry preSet(int virtualDb, String key) {
        this.redisEntryMap.putIfAbsent(virtualDb, new HashMap<>());

        Map<String, SimulatedRedisEntry> entries = this.redisEntryMap.get(virtualDb);

        if (entries.containsKey(key)) {
            return entries.get(key);
        }

        SimulatedRedisEntry newEntry = new SimulatedRedisEntry();
        entries.put(key, newEntry);
        return newEntry;
    }

    //==================================================================================================================

    private SimulatedRedisEntry getEntry(String key) {
        return this.getEntry(0, key);
    }

    private SimulatedRedisEntry getEntry(int virtualDb, String key) {
        if (!this.redisEntryMap.containsKey(virtualDb)) return EMPTY_ENTRY;

        return this.redisEntryMap
                .get(virtualDb)
                .getOrDefault(key, EMPTY_ENTRY);
    }

    //==================================================================================================================

    private final Set<String> EMPTY_SET = new HashSet<>();

    private Set<String> getKeys(String pattern) {
        return this.getKeys(0, pattern);
    }

    private Set<String> getKeys(int virtualDb, String pattern) {
        if(pattern.equalsIgnoreCase("*")) {
            pattern = "";
        }

        Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        if (this.redisEntryMap.containsKey(virtualDb)) {
            Set<String> results = null;

            Map<String, SimulatedRedisEntry> entries = this.redisEntryMap.get(virtualDb);
            for (String key : entries.keySet()) {
                if (compiledPattern.matcher(key).find()) {
                    if (results == null) results = new HashSet<>();

                    results.add(key);
                }
            }

            return results != null ? results : EMPTY_SET;
        }

        return EMPTY_SET;
    }

    //==================================================================================================================

    @Override
    public void set(String key, String value) {
        this.preSet(key).setObjectValue(value);
    }

    @Override
    public void set(int db, String key, String value) {
        this.preSet(db, key).setObjectValue(value);
    }

    @Override
    public String get(String key) {
        return this.getEntry(key).getAsString();
    }

    @Override
    public String get(int db, String key) {
        return this.getEntry(db, key).getAsString();
    }

    @Override
    public void hset(String key, String hashKey, String hashValue) {
        this.preSet(key)
                .getMapAndCreateIfNotExists()
                .put(hashKey, hashValue);
    }

    @Override
    public void hset(int db, String key, String hashKey, String hashValue) {
        this.preSet(db, key)
                .getMapAndCreateIfNotExists()
                .put(hashKey, hashValue);
    }

    @Override
    public void hset(String key, Map<String, String> map) {
        this.preSet(key)
                .setMapValue(map);
    }

    @Override
    public void hset(int db, String key, Map<String, String> map) {
        this.preSet(db, key)
                .setMapValue(map);
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        return this.preSet(key)
                .getMapValue();
    }

    @Override
    public Map<String, String> hgetAll(int db, String key) {
        return this.preSet(db, key)
                .getMapValue();
    }

    @Override
    public String hget(String key, String hashKey) {
        return this.preSet(key)
                .getMapKey(hashKey);
    }

    @Override
    public String hget(int db, String key, String hashKey) {
        return this.preSet(db, key)
                .getMapKey(hashKey);
    }

    @Override
    public void setObject(String key, Object value) {
        this.preSet(key)
                .setObjectValue(value);
    }

    @Override
    public void setObject(int db, String key, Object value) {
        this.preSet(db, key)
                .setObjectValue(value);
    }

    @Override
    public void setObject(String key, Object value, long ttl) {
        SimulatedRedisEntry entry = this.preSet(key);
        entry.setObjectValue(value);
        entry.expireIn(Duration.ofSeconds(ttl));
    }

    @Override
    public void setObject(int db, String key, Object value, long ttl) {
        SimulatedRedisEntry entry = this.preSet(db, key);
        entry.setObjectValue(value);
        entry.expireIn(Duration.ofSeconds(ttl));
    }

    @Override
    public <E> E getObject(String key, Class<E> clazz) {
        return this.preSet(key)
                .getObjectValue(clazz);
    }

    @Override
    public <E> E getObject(int db, String key, Class<E> clazz) {
        return this.preSet(db, key)
                .getObjectValue(clazz);
    }

    @Override
    public void expire(String key, long ttl) {
        this.preSet(key).expireIn(Duration.ofSeconds(ttl));
    }

    @Override
    public void expire(int db, String key, long ttl) {
        this.preSet(db, key).expireIn(Duration.ofSeconds(ttl));
    }

    @Override
    public Set<String> keys(String pattern) {
        return this.getKeys(pattern);
    }

    @Override
    public Set<String> keys(int db, String pattern) {
        return this.getKeys(db, pattern);
    }

    @Override
    public void publish(String channel, String message) {
        //Just used for internal logic
    }

    @Override
    public void stop() {
        this.evictionTimer.cancel();
    }
}
