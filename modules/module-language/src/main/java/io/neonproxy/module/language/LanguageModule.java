package io.neonproxy.module.language;

import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.module.language.api.ILanguageModule;
import io.neonproxy.module.language.api.LanguageAPI;
import io.neonproxy.module.language.impl.LanguageAPIIMpl;
import io.neonproxy.module.language.model.ModelLanguageConfig;
import io.neonproxy.common.module.ModuleLifecycle;
import io.neonproxy.common.module.api.LifecycleHook;
import io.neonproxy.common.config.IConfiguration;
import lombok.Getter;

import java.io.File;
import java.util.Locale;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public class LanguageModule extends AbstractModule implements ILanguageModule {

    public static File LANGUAGE_ROOT;

    //*=====================--------------------------- Private ----------------------------------=====================*
    private IConfiguration<ModelLanguageConfig> languageModuleConfig;

    //*=====================--------------------------- Exposed ----------------------------------=====================*
    @Getter
    private LanguageAPI languageAPI;


    @LifecycleHook(state = ModuleLifecycle.ENABLED)
    private void onEnable() {
        this.languageModuleConfig = this.prepareConfig("language", ModelLanguageConfig.class)
                .continueIfExists()
                .write(new ModelLanguageConfig());

        LANGUAGE_ROOT = this.getLanguageModuleConfig()
                .getLanguageFolderAsFile();

        if(!LANGUAGE_ROOT.exists()) {
            LANGUAGE_ROOT.mkdirs();
            this.copyDefaultLanguage();
        }

        this.languageAPI = new LanguageAPIIMpl(this);

        this.copyDefaultLanguage();

        this.languageAPI.getLanguageKey(Locale.ENGLISH, "");
    }

    private void copyDefaultLanguage() {
        this.languageAPI.importLanguageFile(
                Locale.ENGLISH,
                "general",
                "lang//general_en"
        );
    }

    public ModelLanguageConfig getLanguageModuleConfig() {
        return this.languageModuleConfig.get();
    }
}
