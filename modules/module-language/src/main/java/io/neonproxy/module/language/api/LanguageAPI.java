package io.neonproxy.module.language.api;

import java.util.Locale;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public interface LanguageAPI {

    void importLanguageFile(Locale locale, String fileName, String resourcePath);

    /**
     * imports a language file from the resources folder into the languages folder.
     * @param locale the locale the file represents
     * @param fileName the file's name
     * @param resourcePath the local resource path
     * @param reload if reload is true, all language files will be reloaded after importing
     */
    void importLanguageFile(Locale locale, String fileName, String resourcePath, boolean reload);

    String getLanguageKey(Locale locale, String key, Object... args);
}
