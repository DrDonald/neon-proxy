package io.neonproxy.module.language.api;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public interface ILanguageModule {

    LanguageAPI getLanguageAPI();
}
