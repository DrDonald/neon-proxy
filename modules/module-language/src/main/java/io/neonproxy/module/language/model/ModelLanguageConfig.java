package io.neonproxy.module.language.model;

import io.neonproxy.module.language.LanguageModule;
import lombok.Data;

import java.io.File;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
@Data
public class ModelLanguageConfig {

    private String defaultLocale = "en";
    private boolean tryAdaptLanguage = true;
    private String languageFolder = "lang//";

    public File getLanguageFolderAsFile() {
        return new File(LanguageModule.LANGUAGE_ROOT, this.languageFolder);
    }
}
