package io.neonproxy.module.language.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.neonproxy.module.language.LanguageModule;
import io.neonproxy.module.language.api.LanguageAPI;
import io.neonproxy.common.config.GsonHelper;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jasper Stritzke
 * @since 30.07.21
 */
public class LanguageAPIIMpl implements LanguageAPI {

    private final LanguageModule languageModule;

    // Locale - Map<Name, JsonObject>
    private final Map<Locale, Map<String, JsonObject>> languageData;

    public LanguageAPIIMpl(LanguageModule languageModule) {
        this.languageModule = languageModule;

        this.languageData = new ConcurrentHashMap<>();
        this.loadLanguageFiles();
    }

    private void loadLanguageFiles() {
        this.languageData.clear();

        for (File folder : Objects.requireNonNull(
                this.languageModule.getLanguageModuleConfig()
                        .getLanguageFolderAsFile()
                        .listFiles(File::isDirectory))
        ) {
            for (File jsonFile : Objects.requireNonNull(folder.listFiles(f -> f.getName().endsWith(".json")))) {
                this.loadLanguageFile(Locale.forLanguageTag(folder.getName()), jsonFile);
            }
        }
    }

    @Override
    public void importLanguageFile(Locale locale, String fileName, String resourcePath) {
        this.importLanguageFile(locale, fileName, resourcePath, false);
    }

    @Override
    public void importLanguageFile(Locale locale, String fileName, String resourcePath, boolean reload) {
        try {
            File file = new File(
                    this.languageModule.getLanguageModuleConfig().getLanguageFolderAsFile(),
                    locale.getLanguage() + "//" + fileName + (fileName.endsWith(".json") ? "" : ".json")
            );

            //Language is already imported
            if (file.exists()) return;

            try (InputStream inputStream = this.languageModule.getResource(resourcePath)) {
                //Create folder(s) and file if they don't already exist.
                if (file.getParentFile() != null) file.getParentFile().mkdirs();
                file.createNewFile();

                FileUtils.copyInputStreamToFile(inputStream, file);
            }

            if (reload) this.loadLanguageFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private void loadLanguageFile(Locale locale, File jsonFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(jsonFile))) {
            JsonObject language = GsonHelper.PRETTY_GSON.fromJson(reader, JsonObject.class);

            //If language file has a attribute called 'name' set it as system name. Keys can be accessed with *name*.*key*
            String name = "general";
            if (language.has("name")) {
                JsonElement nameElement = language.get("name");
                if (!nameElement.isJsonPrimitive()) return;

                name = nameElement.getAsString();
                name = name.isBlank() ? "general" : name + ".";
            }

            Map<String, JsonObject> languageKeys = this.languageData.getOrDefault(locale, new ConcurrentHashMap<>());

            String prefix = language.has("prefix") ? language.get("prefix").getAsString() : "";

            languageKeys.put(name, language);

            for (String key : language.keySet()) {
                String value = language.get(key)
                        .getAsString()
                        .replace("%prefix%", prefix);
                language.addProperty(key, value);
            }

            this.languageData.put(locale, languageKeys);
        }

    }

    private void replacePrefix(JsonObject jsonObject, String before, String after) {
        for (String key : jsonObject.keySet()) {
            if (jsonObject.get(key).isJsonObject()) {
                this.replacePrefix(jsonObject.get(key).getAsJsonObject(), before, after);
                continue;
            }

            jsonObject.addProperty(
                    key,
                    jsonObject.get(key)
                            .getAsString()
                            .replace(before, after)
            );
        }
    }

    @Override
    public String getLanguageKey(Locale locale, String key, Object... args) {
        Map<String, JsonObject> localeMessages = this.languageData.get(locale);

        String result;
        if (localeMessages != null) {
            String name = key.split("\\.")[0];

            JsonObject jsonObject = localeMessages.get(name);

            if (jsonObject == null) {
                if (name.equals("general") && !locale.getLanguage().equals(
                        this.languageModule.getLanguageModuleConfig().getDefaultLocale())) {
                    return this.getLanguageKey(
                            Locale.forLanguageTag(
                                    this.languageModule.getLanguageModuleConfig().getDefaultLocale()
                            ), key, args
                    );
                }
                return this.getLanguageKey(locale, "general" + "." + key, args);
            }

            int firstDot = key.indexOf('.');
            String realKey = key.substring(firstDot + 1);

            result = GsonHelper.fromPathAsString(jsonObject, realKey);
            if (result != null) return result;
        }

        return "key " + key + " not found :(";
    }
}
