package io.neonproxy.module.notification.impl.packet;

import io.neonproxy.module.notification.impl.NotificationImpl;
import io.neonproxy.module.network.channel.PubSubPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
@Getter
@AllArgsConstructor
public class PacketNotificationDispatch extends PubSubPacket {

    private final String groupName;

    private final NotificationImpl notification;
}
