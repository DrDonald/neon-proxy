package io.neonproxy.module.notification.impl;

import com.google.common.base.Preconditions;
import io.neonproxy.module.language.api.LanguageAPI;
import io.neonproxy.module.notification.api.NotificationContext;
import io.neonproxy.module.notification.api.NotificationContextOptions;

import java.io.Serializable;
import java.util.Locale;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public class NotificationContextImpl implements NotificationContext, Serializable {

    private String hoverContent = null;
    private boolean hoverRaw = false;
    private Object[] hoverPlaceHolders = null;

    private String clickContent = null;
    private NotificationContextOptions.ClickEvent clickEvent = null;

    private String content = null;
    private boolean raw = false;
    private Object[] placeHolders = null;

    @Override
    public boolean isRawMessage() {
        return this.raw;
    }

    @Override
    public boolean hasHoverAction() {
        return this.hoverContent != null;
    }

    @Override
    public boolean hasClickAction() {
        return this.clickContent != null;
    }

    @Override
    public NotificationContextOptions.ClickEvent getClickEvent() {
        return this.clickEvent;
    }

    @Override
    public NotificationContext setClickAction(NotificationContextOptions.ClickEvent event, String context) {
        Preconditions.checkNotNull(event, "event can not be null!");
        Preconditions.checkNotNull(context, "context can not be null!");

        this.clickEvent = event;
        this.clickContent = context;
        return this;
    }

    @Override
    public NotificationContext setContent(String content, Object... placeHolders) {
        Preconditions.checkNotNull(content, "content can not be null!");

        this.content = content;
        this.placeHolders = placeHolders;
        return this;
    }

    @Override
    public NotificationContext setRawContent(String content) {
        this.raw = true;
        return this.setContent(content);
    }

    @Override
    public NotificationContext setHoverContentRaw(String content) {
        this.hoverRaw = true;
        return this.setHoverContent(content);
    }

    @Override
    public NotificationContext setHoverContent(String content, Object... placeholders) {
        Preconditions.checkNotNull(content, "content can not be null!");
        Preconditions.checkNotNull(placeholders, "placeholders can not be null!");

        this.hoverPlaceHolders = placeHolders;
        this.hoverContent = content;
        return this;
    }

    @Override
    public Object[] getHoverPlaceholders() {
        return this.hoverPlaceHolders;
    }

    @Override
    public boolean isHoverContentRaw() {
        return this.hoverRaw;
    }

    @Override
    public NotificationContext setPlaceHolders(Object... placeHolders) {
        Preconditions.checkNotNull(placeHolders, "placeHolders can not be null!");

        this.placeHolders = placeHolders;
        return this;
    }

    @Override
    public Object[] getPlaceHolders() {
        return this.placeHolders;
    }

    @Override
    public String getClickContext() {
        return this.clickContent;
    }

    @Override
    public String getHoverContent() {
        return this.hoverContent;
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public String format(LanguageAPI languageAPI, Locale locale) {
        return languageAPI.getLanguageKey(locale, this.content);
    }

    @Override
    public boolean validate() {
        return this.content != null;
    }
}
