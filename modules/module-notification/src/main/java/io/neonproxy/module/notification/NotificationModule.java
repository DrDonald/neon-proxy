package io.neonproxy.module.notification;

import io.neonproxy.common.module.ModuleLifecycle;
import io.neonproxy.common.module.api.AbstractModule;
import io.neonproxy.common.module.api.LifecycleHook;
import io.neonproxy.module.notification.api.NotificationManager;
import io.neonproxy.module.notification.api.service.INotificationModule;
import io.neonproxy.module.notification.impl.NotificationManagerImpl;
import io.neonproxy.module.network.api.INetworkModule;
import lombok.Getter;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
@Getter
public class NotificationModule extends AbstractModule implements INotificationModule {

    private NotificationManager notificationManager;
    private INetworkModule networkModule;

    @LifecycleHook(state = ModuleLifecycle.ENABLED)
    private void handleEnable() {
        this.networkModule = this.getModuleManager()
                .getModuleByInterface(INetworkModule.class)
                .orElseThrow();

        this.notificationManager = new NotificationManagerImpl(this.networkModule);
    }
}
