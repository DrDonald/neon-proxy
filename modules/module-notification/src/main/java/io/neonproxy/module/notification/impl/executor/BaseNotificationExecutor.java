package io.neonproxy.module.notification.impl.executor;

import io.neonproxy.module.notification.impl.packet.PacketNotificationDispatch;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
public interface BaseNotificationExecutor {

    void execute(PacketNotificationDispatch packetNotificationDispatch);
}
