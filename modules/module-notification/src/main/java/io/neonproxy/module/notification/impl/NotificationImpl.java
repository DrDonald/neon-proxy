package io.neonproxy.module.notification.impl;

import com.google.common.base.Preconditions;
import io.neonproxy.module.notification.api.NotificationContext;
import io.neonproxy.module.notification.api.NotificationContextOptions;
import io.neonproxy.module.notification.impl.packet.PacketNotificationDispatch;
import io.neonproxy.module.notification.api.Notification;
import io.neonproxy.module.notification.impl.util.NotifyModuleConstants;
import io.neonproxy.module.network.channel.IPubSubManager;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
@RequiredArgsConstructor
public class NotificationImpl implements Notification, Serializable {

    private final IPubSubManager redisPubSubManager;

    private final LinkedList<NotificationContextImpl> notificationContexts = new LinkedList<>();

    private boolean raw = false;

    private NotificationContextOptions.TitleOptions titleOptions = null;
    private NotificationContextOptions.ContextPosition contextPosition = NotificationContextOptions.ContextPosition.MESSAGE;

    private final String groupName;

    @Override
    public void forEach(Consumer<NotificationContext> contextConsumer) {
        this.notificationContexts.forEach(contextConsumer);
    }

    @Override
    public String toString(Function<NotificationContext, String> callback, String separator) {
        if (separator == null) {
            separator = "";
        }

        final StringBuilder stringBuilder = new StringBuilder();

        for (final NotificationContext notificationContext : this.notificationContexts) {
            stringBuilder.append(callback.apply(notificationContext)).append(separator);
        }

        return stringBuilder.toString();
    }

    @Override
    public int getContextSize() {
        return this.notificationContexts.size();
    }

    @Override
    public NotificationContext getAtIndex(int index) {
        return this.notificationContexts.get(index);
    }

    @Override
    public Notification addContext(NotificationContext... contexts) {
        for (NotificationContext context : contexts) {
            if (context instanceof NotificationContextImpl) {
                this.notificationContexts.add((NotificationContextImpl) context);
            }
        }

        return this;
    }

    @Override
    public boolean isTitle() {
        return this.titleOptions != null && this.contextPosition == NotificationContextOptions.ContextPosition.TITLE;
    }

    @Override
    public NotificationContextOptions.ContextPosition getPosition() {
        return this.contextPosition;
    }

    @Override
    public Notification setPosition(NotificationContextOptions.ContextPosition position) {
        Preconditions.checkNotNull(position, "position can not be null!");

        this.contextPosition = position;
        return this;
    }

    @Override
    public NotificationContextOptions.TitleOptions getTitleOptions() {
        return this.titleOptions;
    }

    @Override
    public Notification setTitle(NotificationContextOptions.TitleOptions titleOptions) {
        Preconditions.checkNotNull(titleOptions, "titleOptions can not be null!");

        this.titleOptions = titleOptions;
        this.setPosition(NotificationContextOptions.ContextPosition.TITLE);
        return this;
    }

    @Override
    public NotificationContext prepareContext() {
        return new NotificationContextImpl();
    }

    @Override
    public CompletableFuture<Void> submit() {
        return redisPubSubManager.dispatchPacket(
                NotifyModuleConstants.NOTIFICATION_CHANNEL,
                new PacketNotificationDispatch(
                        this.groupName,
                        this
                )
        );
    }
}
