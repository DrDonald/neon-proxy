package io.neonproxy.module.notification.impl.executor;

import io.neonproxy.module.language.api.LanguageAPI;
import io.neonproxy.module.notification.api.*;
import io.neonproxy.module.notification.impl.packet.PacketNotificationDispatch;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;

import java.text.MessageFormat;

/**
 * @author Jasper Stritzke
 * @since 07.08.21
 */
@AllArgsConstructor
public class BungeeCordExecutor implements BaseNotificationExecutor, Listener {

    private final NotificationManager notificationManager;
    private final LanguageAPI languageAPI;

    @Override
    public void execute(PacketNotificationDispatch packet) {
        NotificationGroup notificationGroup = this.notificationManager.getNotificationGroup(packet.getGroupName());

        if (notificationGroup != null) {
            Notification notification = packet.getNotification();

            if (notification.isTitle() && notification.getContextSize() >= 2) {
                NotificationContextOptions.TitleOptions titleOptions = notification.getTitleOptions();

                NotificationContext firstLine = notification.getAtIndex(0);
                NotificationContext secondLine = notification.getAtIndex(1);

                Title title = ProxyServer.getInstance().createTitle();

                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (player.hasPermission(notificationGroup.getRequiredPermission())) {
                        TextComponent first = this.translateTextComponent(firstLine, player);
                        TextComponent second = this.translateTextComponent(secondLine, player);

                        title
                                .fadeIn(titleOptions.getFadeIn())
                                .stay(titleOptions.getStay())
                                .fadeOut(titleOptions.getFadeOut())
                                .title(first)
                                .subTitle(second)
                                .send(player);

                        title.reset();
                    }
                }
                return;
            }

            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                if (player.hasPermission(notificationGroup.getRequiredPermission())) {
                    TextComponent parent = new TextComponent();
                    notification.forEach(context -> parent.addExtra(this.translateTextComponent(context, player)));

                    switch (notification.getPosition()) {
                        case MESSAGE:
                            player.sendMessage(parent);
                            break;
                        case ACTION_BAR:
                            player.sendMessage(ChatMessageType.ACTION_BAR, parent);
                            break;
                        case TITLE:
                            player.sendMessage("§cSomething went wrong...");
                            break;
                    }
                }
            }
        }
    }

    private TextComponent translateTextComponent(NotificationContext notificationContext, ProxiedPlayer proxiedPlayer) {
        TextComponent textComponent;

        if (notificationContext.isRawMessage()) {
            textComponent = new TextComponent(notificationContext.getContent());
        } else {
            textComponent = new TextComponent(notificationContext.format(this.languageAPI, proxiedPlayer.getLocale()));
        }

        if (notificationContext.hasClickAction()) {
            textComponent.setClickEvent(
                    new ClickEvent(
                            this.translate(notificationContext.getClickEvent()),
                            notificationContext.getClickContext()
                    )
            );
        }

        if (notificationContext.hasHoverAction()) {
            textComponent.setHoverEvent(
                    new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                            notificationContext.isHoverContentRaw() ?
                                    notificationContext.getHoverContent() :
                                    MessageFormat.format(
                                            this.languageAPI.getLanguageKey(proxiedPlayer.getLocale(), notificationContext.getHoverContent()),
                                            notificationContext.getHoverPlaceholders()
                                    )
                    ))
            );
        }

        return textComponent;
    }

    private ClickEvent.Action translate(NotificationContextOptions.ClickEvent event) {
        switch (event) {
            case RUN_COMMAND:
                return ClickEvent.Action.RUN_COMMAND;
            case OPEN_URL:
                return ClickEvent.Action.OPEN_URL;
            case PREPARE_COMMAND:
                return ClickEvent.Action.SUGGEST_COMMAND;
            default:
                return null;
        }
    }
}
