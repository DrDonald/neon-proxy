package io.neonproxy.module.notification.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public class NotificationContextOptions implements Serializable {

    public enum ContextPosition implements Serializable {

        MESSAGE,
        TITLE,
        ACTION_BAR
    }

    public enum ClickEvent implements Serializable {

        RUN_COMMAND,
        PREPARE_COMMAND,
        OPEN_URL
    }

    @Getter
    @RequiredArgsConstructor
    public static class TitleOptions implements Serializable {

        public static final TitleOptions DEFAULT = new TitleOptions(5, 30, 5);

        private final int fadeIn, stay, fadeOut;
    }
}
