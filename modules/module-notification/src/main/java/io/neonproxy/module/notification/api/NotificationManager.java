package io.neonproxy.module.notification.api;

import java.util.List;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public interface NotificationManager {

    List<NotificationGroup> getNotificationGroups();

    NotificationGroup registerNotificationGroup(String name, String displayName, String permission);

    NotificationGroup getNotificationGroup(String name);
    NotificationGroup getOrCreateNotificationGroup(String name, String displayName, String permission);
}
