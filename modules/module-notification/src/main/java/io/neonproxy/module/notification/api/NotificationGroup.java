package io.neonproxy.module.notification.api;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public interface NotificationGroup {

    String getName();

    String getDisplayName();

    String getRequiredPermission();

    Notification prepareNotification();
    void dispatchNotification(Notification notification);
}
