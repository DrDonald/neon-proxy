package io.neonproxy.module.notification.impl;

import com.google.common.base.Preconditions;
import io.neonproxy.module.notification.api.Notification;
import io.neonproxy.module.notification.api.NotificationGroup;
import io.neonproxy.module.notification.impl.packet.PacketNotificationDispatch;
import io.neonproxy.module.notification.impl.util.NotifyModuleConstants;
import io.neonproxy.module.network.channel.IPubSubManager;
import lombok.AllArgsConstructor;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
@AllArgsConstructor
public class NotificationGroupImpl implements NotificationGroup {

    private final IPubSubManager pubSubManager;
    private final String name, displayName, permission;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public String getRequiredPermission() {
        return this.permission;
    }

    @Override
    public Notification prepareNotification() {
        return new NotificationImpl(this.pubSubManager, this.getName());
    }

    @Override
    public void dispatchNotification(final Notification notification) {
        Preconditions.checkNotNull(notification, "notificationContext can not be null!");

        if (!(notification instanceof NotificationImpl)) {
            throw new IllegalArgumentException("notificationContext is not an instance of notificationContextImpl. Please create a notification with NotificationGroup#prepareNotificationContext");
        }

        final NotificationImpl notificationImpl = (NotificationImpl) notification;

        this.pubSubManager.dispatchPacket(
                NotifyModuleConstants.NOTIFICATION_CHANNEL,
                new PacketNotificationDispatch(
                        this.getName(),
                        notificationImpl
                )
        );
    }

    private String getKey() {
        return "notifyState:" + this.getName();
    }
}
