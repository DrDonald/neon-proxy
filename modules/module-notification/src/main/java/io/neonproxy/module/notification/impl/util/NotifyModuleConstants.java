package io.neonproxy.module.notification.impl.util;

/**
 * @author Jasper Stritzke
 * @since 15.04.21
 */
public class NotifyModuleConstants {

    public static final int REDIS_DB = 4;
    public static final String NOTIFICATION_CHANNEL = "NOTIFY_SERVICE_CHANNEL";
}
