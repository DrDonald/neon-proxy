package io.neonproxy.module.notification.api;

import io.neonproxy.module.language.api.LanguageAPI;

import java.util.Locale;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public interface NotificationContext {

    /**
     * If message contains raw message or language string
     */
    boolean isRawMessage();

    boolean hasHoverAction();

    boolean hasClickAction();

    NotificationContextOptions.ClickEvent getClickEvent();
    NotificationContext setClickAction(NotificationContextOptions.ClickEvent event, String context);

    NotificationContext setContent(String content, Object... placeholders);
    NotificationContext setRawContent(String content);

    NotificationContext setHoverContentRaw(String content);

    NotificationContext setHoverContent(String content, Object... placeholders);
    Object[] getHoverPlaceholders();

    boolean isHoverContentRaw();

    NotificationContext setPlaceHolders(Object... placeHolders);
    Object[] getPlaceHolders();

    String getClickContext();
    String getHoverContent();

    String getContent();

    String format(LanguageAPI languageAPI, Locale locale);

    boolean validate();
}
