package io.neonproxy.module.notification.api;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public interface Notification {

    void forEach(Consumer<NotificationContext> contextConsumer);

    /**
     *
     * @param separator if null : ""
     */
    String toString(Function<NotificationContext, String> callback, String separator);

    int getContextSize();

    NotificationContext getAtIndex(int index);

    Notification addContext(NotificationContext... contexts);

    /**
     * Use \n for second line
     */
    boolean isTitle();

    NotificationContextOptions.ContextPosition getPosition();
    Notification setPosition(NotificationContextOptions.ContextPosition position);

    NotificationContextOptions.TitleOptions getTitleOptions();
    Notification setTitle(NotificationContextOptions.TitleOptions titleOptions);

    NotificationContext prepareContext();

    CompletableFuture<Void> submit();
}
