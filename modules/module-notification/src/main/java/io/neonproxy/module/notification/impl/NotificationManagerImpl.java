package io.neonproxy.module.notification.impl;


import io.neonproxy.module.notification.api.NotificationGroup;
import io.neonproxy.module.notification.api.NotificationManager;
import io.neonproxy.module.notification.impl.packet.PacketNotificationDispatch;
import io.neonproxy.module.notification.impl.packet.PacketNotificationGroupUpdate;
import io.neonproxy.module.notification.impl.util.NotifyModuleConstants;
import io.neonproxy.module.network.api.INetworkModule;
import io.neonproxy.module.network.api.PacketHandler;
import io.neonproxy.module.network.channel.IPubSubChannel;
import io.neonproxy.module.network.channel.IPubSubManager;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jasper Stritzke
 * @since 06.04.21
 */
public class NotificationManagerImpl implements NotificationManager {

    private final INetworkModule networkModule;

    private final List<NotificationGroup> groups;

    private volatile IPubSubChannel channel;

    public NotificationManagerImpl(INetworkModule networkModule) {
        this.networkModule = networkModule;

        IPubSubManager pubSubManager = this.networkModule.getPubSubManager();

        pubSubManager.registerPacket(PacketNotificationDispatch.class);
        pubSubManager.registerPacket(PacketNotificationGroupUpdate.class);
        pubSubManager.registerChannel(
                this,
                NotifyModuleConstants.NOTIFICATION_CHANNEL
        ).whenComplete((pubSubChannel, throwable) -> this.channel = pubSubChannel);

        this.groups = new LinkedList<>();

        this.refreshGroups();
    }

    @Override
    public List<NotificationGroup> getNotificationGroups() {
        return this.groups;
    }

    @Override
    public NotificationGroup registerNotificationGroup(String rawName, final String displayName, final String permission) {
        final String name = rawName.toLowerCase();

        final NotificationGroup oldGroup = this.getNotificationGroup(name);

        if (oldGroup != null) {
            return oldGroup;
        }

        final Map<String, String> map = new ConcurrentHashMap<>();
        map.put("name", name);
        map.put("displayName", displayName);
        map.put("permission", permission);

        this.networkModule.getRedisManager().hset("notifyGroup:" + name, map);

        final NotificationGroup notificationGroup = new NotificationGroupImpl(this.networkModule.getPubSubManager(), name, displayName, permission);

        this.groups.add(notificationGroup);

        this.update();

        return notificationGroup;
    }

    private void update() {
        this.channel.dispatchPacket(new PacketNotificationGroupUpdate());
    }

    @Override
    public NotificationGroup getNotificationGroup(String name) {
        name = name.toLowerCase();

        for (final NotificationGroup group : this.groups) {
            if (group.getName().equals(name)) {
                return group;
            }
        }

        return null;
    }

    @Override
    public NotificationGroup getOrCreateNotificationGroup(String name, String displayName, String permission) {
        name = name.toLowerCase();

        final NotificationGroup group = this.getNotificationGroup(name);

        if (group != null) {
            return group;
        }

        return this.registerNotificationGroup(name, displayName, permission);
    }

    @PacketHandler
    public void handle(final PacketNotificationGroupUpdate packet) {
        this.refreshGroups();
    }

    private boolean didChange() {
        return this.groups.size() != this.networkModule.getRedisManager()
                .keys(NotifyModuleConstants.REDIS_DB, "notifyGroup:*")
                .size();
    }

    private void refreshGroups() {
        if (!this.didChange()) return;

        this.groups.clear();

        this.networkModule.getRedisManager().keys(NotifyModuleConstants.REDIS_DB, "notifyGroup:*")
                .forEach(key -> {
                    final Map<String, String> map = this.networkModule.getRedisManager().hgetAll(key);

                    this.groups.add(
                            new NotificationGroupImpl(
                                    this.networkModule.getPubSubManager(),
                                    map.get("name"),
                                    map.get("displayName"),
                                    map.get("permission")
                            )
                    );
                });
    }
}
