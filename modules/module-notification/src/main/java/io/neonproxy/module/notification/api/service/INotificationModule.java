package io.neonproxy.module.notification.api.service;

import io.neonproxy.module.notification.api.NotificationManager;

/**
 * @author Jasper Stritzke
 * @since 15.04.21
 */
public interface INotificationModule {

    NotificationManager getNotificationManager();
}
